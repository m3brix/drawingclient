import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserImage } from '../Classes/UserImage';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  fileData: any
  selectedUserImage:UserImage=new UserImage();
  constructor(private http:HttpClient,private userService:UserService){ }
  basic_URL="http://localhost:53524/"

  GetImageByUser():Observable<Array<UserImage>>
  {
      return this.http.get<Array<UserImage>>(this.basic_URL+"api/Image/GetImageByUser"+1)
  }
  saveImage(fileData:File, u:UserImage):Observable<string> {
     u.userId=this.userService.user.userId;
    const formData = new FormData();
    formData.append('image',fileData);
    formData.append('userImage',JSON.stringify(u));
    return this.http.post<string>(this.basic_URL+"api/Image/AddImageByUser", formData);
    
  }
  getAllMyImages(userId:number):Observable<Array<UserImage>>
  {
    debugger;
      return this.http.get<Array<UserImage>>(this.basic_URL+"api/Image/GetAllMyImages"+userId)
  }
}
