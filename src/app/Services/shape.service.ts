import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Shape } from '../Classes/Shape';
import { ShapeImage } from '../Classes/ShapeImage';

@Injectable({
  providedIn: 'root'
})
export class ShapeService {

  constructor(private http:HttpClient){ }
  basic_URL="http://localhost:53524/"

  GetShapes():Observable<Array<Shape>>
  {
    return this.http.get<Array<Shape>>(this.basic_URL+"api/Shape/GetAllShapes")
  }
  // AddShape(name:string):Observable<any>
  // {
  //    return this.http.get<any>(this.basic_URL+"api/Shape/GetAddShape"+name)
  // }
  AddShapesToImage(listShapeImages:Array<ShapeImage>):Observable<Array<ShapeImage>>
  {
    return this.http.post<Array<ShapeImage>>(this.basic_URL+"api/Shape/AddShapeToImage",listShapeImages)
    
  }
  GetShapesByImage(userImageId:number):Observable<Array<ShapeImage>>
  {
    return this.http.get<Array<ShapeImage>>(this.basic_URL+"api/Shape/GetShapesByImage"+userImageId)
  }
}
