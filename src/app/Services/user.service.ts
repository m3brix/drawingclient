import { Injectable } from '@angular/core';
import { User } from '../Classes/User';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user:User=new User();
  constructor(private http:HttpClient) { }
  basic_URL="http://localhost:53524/"
  AddUser(user:User):Observable<any>
  {
    return this.http.post<any>(this.basic_URL+"api/User/AddUser",user)
  }
  CheckUser(userName:string,password:string):Observable<User>
  {
    return this.http.post<User>(this.basic_URL+"api/User/CheckUser",[userName,password])
  }
}
