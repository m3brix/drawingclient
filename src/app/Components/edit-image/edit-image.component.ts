import { OnInit } from '@angular/core';

import {
  Component, Input, ElementRef, AfterViewInit, ViewChild
} from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { switchMap, takeUntil, pairwise, buffer } from 'rxjs/operators'
import { map } from 'rxjs/operators';
import { Point } from 'src/app/Classes/Point';

import { User, UserLogin } from 'src/app/Classes/User';
import { UserService } from 'src/app/Services/user.service';
import { ShapeService } from 'src/app/Services/shape.service';
import { ShapeImage } from 'src/app/Classes/ShapeImage';
import { Shape } from 'src/app/Classes/Shape';
// import { Duplex } from 'stream';
import { ImageService } from 'src/app/Services/image.service';
import { UserImage } from 'src/app/Classes/UserImage';
import { Router } from '@angular/router';
@Component({
  selector: 'app-edit-image',
  templateUrl: './edit-image.component.html',
  styleUrls: ['./edit-image.component.css']
})
export class EditImageComponent implements OnInit {

  constructor(private router:Router,private usersService: UserService, private shapeService: ShapeService ,private imageService:ImageService) { }
  file: File
  fileData: any
  previewUrl: any
  fileUploadProgress: string
  uploadedFilePath: string
  name:string
  //פרטי התמונה
  isClick=false
  userimage:UserImage;
  //משתנים עבור חישוב צורה ושמירתה
  center: Point = new Point();
  sumX: number;
  sumY: number;
  maxX: Point;
  minX: Point;
  maxY: Point;
  minY: Point;
  isCircle: boolean = false;
  isSquare: boolean = false;
  isLine: boolean = false;
  shapeImage: ShapeImage = new ShapeImage()
  listShapeImages: Array<ShapeImage> = null
  allShapes:Array<Shape>=null
  shape:Shape
  src:UserImage=new UserImage();

  borderColor: string = "black"
  fillColor: string = "transparent"
  opacity: number = 100
  filling: boolean = false

  @ViewChild('canvas1') public canvas1: ElementRef;
  @ViewChild('canvas2') public canvas2: ElementRef;
  @ViewChild('form') public form: ElementRef;
  @Input() public width = 800;
  @Input() public height = 600;
  polyline = new Subject<Point>()
  private cx: CanvasRenderingContext2D;
  private ctx: CanvasRenderingContext2D;
  private ctx2: CanvasRenderingContext2D;
  //משתנים עבור ציור קו
  startPoint: Point = null;
  endPoint: Point = new Point();
  ngOnInit() {
     this.shape=new Shape()
     this.listShapeImages=new Array<ShapeImage>();
     this.allShapes=new Array<Shape>();
     this.name=this.usersService.user.userName;
     this.isClick=false;
     this.userimage=new UserImage()
    this.sumX = 0
    this.sumY = 0
    this.maxX = new Point(0, 0)
    this.minX = new Point(10000, 0)
    this.maxY = new Point(0, 0)
    this.minY = new Point(0, 10000)
    this.fileData = null
    this.previewUrl = this.imageService.selectedUserImage.pictuer;
    this.src=this.imageService.selectedUserImage;
    this.fileUploadProgress = null
    this.uploadedFilePath = null
    this.GetShapes()
    if(this.imageService.selectedUserImage.userImageId)
     this.GetShapesByImage()
  }


  public ngAfterViewInit() {
    const canvasEl1: HTMLCanvasElement = this.canvas1.nativeElement;
    const canvasEl2: HTMLCanvasElement = this.canvas2.nativeElement;
    const formEl: HTMLCanvasElement = this.form.nativeElement;
    this.cx = canvasEl1.getContext('2d');
    this.ctx = canvasEl1.getContext('2d');
    this.ctx2 = canvasEl2.getContext('2d');
    canvasEl1.width = this.width;
    canvasEl1.height = this.height;
    canvasEl2.width = this.width;
    canvasEl2.height = this.height;
    //ציור חפשי
    this.cx.lineWidth = 3;
    this.cx.lineCap = 'round';
    this.cx.strokeStyle = 'black'
    this.captureEvents(canvasEl1, formEl);
  }
  private captureEvents(canvasEl1: HTMLCanvasElement, formEl: HTMLCanvasElement) {
    var mouseUpFrom$ = fromEvent(formEl, 'mouseup')
    //buffer שומר את כל הערכים עד ארוע מסוים
    // this.polyline.pipe(buffer(mouseUp$)).subscribe(x=>alert(x))
    this.polyline.pipe(buffer(mouseUpFrom$)).subscribe(data => { if (data.length > 0) this.drawShape(data) })
    fromEvent(canvasEl1, 'mousedown')
      .pipe(
        switchMap((e) => {
          // after a mouse down, we'll record all mouse moves
          return fromEvent(canvasEl1, 'mousemove')
            .pipe(
              // we'll stop (and unsubscribe) once the user releases the mouse
              // this will trigger a 'mouseup' event    
              // takeUntil(fromEvent(canvasEl1, 'mouseup')),
              takeUntil(fromEvent(formEl, 'mouseup')),
              // we'll also stop (and unsubscribe) once the mouse leaves the canvas (mouseleave event)
              // takeUntil(fromEvent(canvasEl, 'mouseleave')),
              // pairwise lets us get the previous value to draw a line from
              // the previous point to the current point    
              pairwise(),
            )
        })
      )
      .subscribe((res: [MouseEvent, MouseEvent]) => {
        const rect = canvasEl1.getBoundingClientRect();
        // previous and current position with the offset
        const prevPos = {
          x: res[0].clientX - rect.left,
          y: res[0].clientY - rect.top
        };

        const currentPos = {
          x: res[1].clientX - rect.left,
          y: res[1].clientY - rect.top
        };
        //אם הצורה היא קו
        if (this.shape.shapeName=='קו') {
          if (this.startPoint == null) {
            this.startPoint = new Point();
            this.startPoint = currentPos;
          }
          this.endPoint = currentPos;
        }
        //לעשות תנאי אם הצורה היא לא קו
        else {
          this.sumX += currentPos.x;
          this.sumY += currentPos.y;
          this.maxX = currentPos.x > this.maxX.x ? currentPos : this.maxX
          this.minX = currentPos.x < this.minX.x ? currentPos : this.minX
          this.maxY = currentPos.y > this.maxY.y ? currentPos : this.maxY
          this.minY = currentPos.y < this.minY.y ? currentPos : this.minY
        }
        // this method we'll implement soon to do the actual drawing
        this.drawOnCanvas(prevPos, currentPos);
        this.polyline.next(new Point(currentPos.x, currentPos.y))
      });

  }

  private drawOnCanvas(prevPos: { x: number, y: number }, currentPos: { x: number, y: number }) {
    if (!this.cx) { return; }

    this.cx.beginPath();

    if (prevPos) {
      this.cx.strokeStyle=this.borderColor;
      this.cx.moveTo(prevPos.x, prevPos.y); // from
      this.cx.lineTo(currentPos.x, currentPos.y);
      this.cx.stroke();
    }
  }
  drawShape(data) {
    this.center.x = this.sumX / data.length
    this.center.y = this.sumY / data.length
    //נקוי הקנבס השקוף...
    this.ctx.clearRect(0, 0, this.canvas2.nativeElement.width, this.canvas2.nativeElement.height);


    //מילוי נתוני הצורה
    this.ctx2.lineWidth = 2;
    this.shapeImage.color = this.ctx2.strokeStyle = this.borderColor;
    if (!this.filling) {
      this.fillColor = "transparent"
    }
    this.shapeImage.fillingColor = this.ctx2.fillStyle = this.fillColor
    this.shapeImage.opacity = this.ctx2.globalAlpha = this.opacity / 100

    this.ctx2.beginPath();

    this.shapeImage.shapeId = this.shape.shapeId
    this.shapeImage.width = (this.maxX.x - this.center.x + this.center.x - this.minX.x) / 2
    this.shapeImage.height = (this.maxY.y - this.center.y + this.center.y - this.minY.y) / 2
    // alert("w:"+width+" h:"+height)
    // ציור ריבוע כולל מלוי
    if (this.shape.shapeName == 'ריבוע') {
      this.shapeImage.startX = this.minX.x;
      this.shapeImage.startY = this.minY.y;
      this.ctx2.strokeRect(this.shapeImage.startX, this.shapeImage.startY, this.shapeImage.width * 2, this.shapeImage.height * 2)
      this.ctx2.fillRect(this.shapeImage.startX + 1, this.shapeImage.startY + 1, this.shapeImage.width * 2 - 2, this.shapeImage.height * 2 - 2)
    }
    // //ציור ריבוע ללא מלוי
    // this.ctx2.strokeRect(100,100,50,50);
    //ציור עיגול
    if (this.shape.shapeName == "עיגול") {
      this.shapeImage.centerX = this.center.x;
      this.shapeImage.centerY = this.center.y;
      this.ctx2.ellipse(this.shapeImage.centerX, this.shapeImage.centerY, this.shapeImage.width, this.shapeImage.height, 0, 0, 2 * Math.PI);

    }
    //ציור קו
    if (this.shape.shapeName == "קו") {
      this.shapeImage.startX = this.startPoint.x;
      this.shapeImage.startY = this.startPoint.y;
      this.shapeImage.endX = this.endPoint.x;
      this.shapeImage.endY = this.endPoint.y;

      this.ctx2.moveTo(this.shapeImage.startX, this.shapeImage.startY);
      this.ctx2.lineTo(this.shapeImage.endX, this.shapeImage.endY);
    }
    
    this.ctx2.fill();
    this.ctx2.stroke();
    this.addShape()
  }

  GetShapes() {
    this.shapeService.GetShapes().subscribe(
      (data) =>{ 
      console.log(data)
      this.allShapes=data;
    debugger;}
    )
  }
  addShape()
  {//אתחול המשתנים
    this.sumX = this.sumY = 0
    this.maxX = new Point(0, 0)
    this.minX = new Point(10000, 0)
    this.maxY = new Point(0, 0)
    this.minY = new Point(0, 10000)
    this.startPoint = null;
    this.endPoint = new Point();
    //הוספת צורה לרשימה מקומית
    this.listShapeImages.push(this.shapeImage)
    this.shapeImage = new ShapeImage()
  }


  
  GetShapesByImage() {
    this.shapeService.GetShapesByImage(this.imageService.selectedUserImage.userImageId).subscribe(
      data => { this.listShapeImages = data; this.drawAllShapesOnImage() },
      err => { alert(err.message) })
  }

  drawAllShapesOnImage() {
    
    this.listShapeImages.forEach(x => {
      this.drawShapeOnImage(x)
    });
  }
  drawShapeOnImage(sh: ShapeImage) {
    debugger;
    this.ctx2.lineWidth = 2
    this.ctx2.strokeStyle = sh.color
    this.ctx2.fillStyle = sh.fillingColor
    this.ctx2.globalAlpha = sh.opacity
    var shapename=this.allShapes.find(s=>s.shapeId==sh.shapeId).shapeName
    this.ctx2.beginPath();
    // ציור ריבוע כולל מלוי
    if (shapename== 'ריבוע') {     
      this.ctx2.rect(sh.startX, sh.startY, sh.width * 2, sh.height * 2)
      this.ctx2.fillRect(sh.startX + 1, sh.startY + 1, sh.width * 2 - 2, sh.height * 2 - 2)
    }
    //ציור עיגול
    else if (shapename == "עיגול") {
      this.ctx2.ellipse(sh.centerX, sh.centerY, sh.width, sh.height, 0, 0, 2 * Math.PI);

    }
    //ציור קו
    else if (shapename == "קו") {

      this.ctx2.moveTo(sh.startX, sh.startY);
      this.ctx2.lineTo(sh.endX, sh.endY);
    }
   
    this.ctx2.fill();
    this.ctx2.stroke();
  }
  //קוד של העלאת תמונה
  GetImageByUser()
    {
      debugger;
      this.imageService.GetImageByUser().subscribe(
        (data)=>{debugger; this.src=data[data.length-1];
          this.listShapeImages=new Array<ShapeImage>();
          this.AddShapesToImage(data[data.length-1].userImageId)})
    }
    
    saveImage()
    {
      if(this.src.userImageId)
      this.AddShapesToImage(this.src.userImageId);
else{
    this.imageService.saveImage(this.fileData,this.userimage).subscribe(
    data => { alert("success " + data); 
    this.GetImageByUser()},
      err => { alert(err.message) })
    }
  }
    fileProgress(fileInput: any) {
      this.fileData = <File>fileInput.target.files[0];
      this.preview();
    }
  
    preview() {
      // Show preview 
      var mimeType = this.fileData.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
  
      var reader = new FileReader();
      reader.readAsDataURL(this.fileData);
      reader.onload = (_event) => {
        this.previewUrl = reader.result;
      }
    }
  AddShapesToImage(userImageId:number)
  {
    debugger;
    this.listShapeImages.forEach(x => {
      x.userImageId=userImageId
    });
   //this.shapeImage.userImageId= 
   this.shapeService.AddShapesToImage(this.listShapeImages).subscribe(
     
    data=>{this.listShapeImages = new Array<ShapeImage>()},
    err=>{alert(err.message) }
   )
  }
  goToImagesList()
  {
    this.router.navigate(['/ImagesList'])
  }
  close()
  {
    this.previewUrl=null;
    this.listShapeImages=new Array<ShapeImage>();
    
  }
  closeForm()
  {
    this.isClick=false
  }

}
