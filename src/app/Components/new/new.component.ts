import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Classes/User';
import { UserService } from 'src/app/Services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  constructor(private userService:UserService,private router:Router) { }
newUser:User=new User();

  ngOnInit() {
    
  }
  AddUser()
  {
      debugger;
      this.userService.AddUser(this.newUser).subscribe(
        (data)=>{debugger;this.router.navigate(['/Login'])})
  }

}
