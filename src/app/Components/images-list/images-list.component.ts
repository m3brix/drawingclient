import { Component, OnInit } from '@angular/core';
import { ImageService } from 'src/app/Services/image.service';
import { UserService } from 'src/app/Services/user.service';
import { UserImage } from 'src/app/Classes/UserImage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.css']
})
export class ImagesListComponent implements OnInit {

  constructor(private imageService:ImageService,private usersService:UserService,private router:Router) { }
images:Array<UserImage>=new Array<UserImage>();
  ngOnInit() {
    this.getAllMyImages()
  }
  getAllMyImages()
  {
    debugger;
    this.imageService.getAllMyImages(this.usersService.user.userId).subscribe(
      (data)=>{debugger;this.images=data; })
  }
  goToEdit(userImage:UserImage)
  {
    this.imageService.selectedUserImage=userImage;
    debugger;
    this.router.navigate(['/EditImage'])
  }
}
