export class User {
    userId:number;
    lastName: string
    firstName: string
    userName: string
    password: string
}
export class UserLogin {
    userName: string
    password: string
}