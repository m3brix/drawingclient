export class ShapeImage {
    shapeImageId?: number
    userImageId: number
    shapeId: number
    centerX: number
    centerY: number
    width: number
    height: number
    color: string
    fillingColor: string
    opacity: number
    angle: number
    shapeTxt: string
    startX: number
    startY: number
    endX: number
    endY: number
    /**
     *
     */
    constructor() {
        this.centerX=null
        this.centerY=null
        this.width=null;
        this.height=null
        this.color=null
        this.fillingColor=null
        this.opacity=null
        this.angle=null
        this.shapeTxt=null
        this.startX=null
        this.startY=null
        this. endX=null
        this.endY=null
    }
} 