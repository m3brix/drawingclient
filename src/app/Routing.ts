import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { EditImageComponent } from './Components/edit-image/edit-image.component';
import { ImagesListComponent } from './Components/images-list/images-list.component';
import { ImageDetailsComponent } from './Components/image-details/image-details.component';
import { NewComponent } from './Components/new/new.component';

export const routDefinition = [
    {
        path: 'Login',
        component: LoginComponent,
    },
    {
        path: 'EditImage',
        component: EditImageComponent,
    },
    {
        path: 'ImagesList',
        component: ImagesListComponent,
    },
    {
        path: 'New',
        component: NewComponent,
    },
    {
        path:'ImageDetails',
        component:ImageDetailsComponent
    },
    
    {
        path: '',
        redirectTo: 'Login',
        pathMatch:'full'
    },
    {
        path: '**',
        component:AppComponent ,
    }
]