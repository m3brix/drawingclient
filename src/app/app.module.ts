import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { routDefinition } from './Routing';
import { LoginComponent } from './Components/login/login.component';
import { ImagesListComponent } from './Components/images-list/images-list.component';
import { EditImageComponent } from './Components/edit-image/edit-image.component';
import { ImageDetailsComponent } from './Components/image-details/image-details.component';
import { NewComponent } from './Components/new/new.component';
// import {TooltipModule} from 'primeng/tooltip';
// import {ColorPickerModule} from 'primeng/colorpicker';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ImagesListComponent,
    EditImageComponent,
    ImageDetailsComponent,
    NewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    // TooltipModule,
    // ColorPickerModule,
    RouterModule.forRoot(routDefinition, {enableTracing: true})  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
